resource "google_service_account" "backup-script-service-account" {
  account_id   = "backup-script-service-account"
  display_name = "Backup Service Account"
  description  = "Used to authenticate backup-script.sh to backup bucket"
}

resource "google_project_iam_binding" "backup-sa-role" {
  role = "roles/iam.serviceAccountUser"
  members = [
    "serviceAccount:${google_service_account.backup-script-service-account.email}",
  ]
}

resource "google_project_iam_binding" "backup-storage-admin-role" {
  role = "roles/storage.admin"
  members = [
    "serviceAccount:${google_service_account.backup-script-service-account.email}",
  ]
}

resource "google_storage_bucket" "home-server-backup" {
  name                        = "aarnihalinen-home-server-backup"
  location                    = "EU"
  force_destroy               = true
  storage_class               = "ARCHIVE"
  uniform_bucket_level_access = true

  versioning {
    enabled = true
  }
  lifecycle_rule {
    condition {
      age        = 30
      with_state = "ARCHIVED"
    }
    action {
      type = "Delete"
    }
  }
}

data "google_storage_project_service_account" "backup-notification-sa" {}

data "archive_file" "function_archive" {
  type        = "zip"
  source_dir  = "dist/backup-notify"
  output_path = "index.zip"
}

resource "google_pubsub_topic" "backup-topic" {
  name = "backup-complete-topic"
}

resource "google_pubsub_topic_iam_binding" "binding" {
  topic   = google_pubsub_topic.backup-topic.id
  role    = "roles/pubsub.publisher"
  members = ["serviceAccount:${data.google_storage_project_service_account.backup-notification-sa.email_address}"]
}

resource "google_storage_notification" "backup-notification" {
  bucket         = google_storage_bucket.home-server-backup.name
  payload_format = "JSON_API_V1"
  topic          = google_pubsub_topic.backup-topic.id
  depends_on     = [google_pubsub_topic_iam_binding.binding]
}

resource "google_storage_bucket" "cf-bucket" {
  name = "cloudfunction-source-bucket"
}

resource "google_storage_bucket_object" "archive" {
  name   = "${data.archive_file.function_archive.output_md5}.zip"
  bucket = google_storage_bucket.cf-bucket.name
  source = data.archive_file.function_archive.output_path
}

resource "google_cloudfunctions_function" "backup-tg-function" {
  name                  = "backup-tg-function"
  description           = "Reads in pub_sub and sends POST to TG"
  runtime               = "nodejs14"
  region                = "europe-west1"
  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.cf-bucket.name
  source_archive_object = google_storage_bucket_object.archive.name

  event_trigger {
    event_type = "providers/cloud.pubsub/eventTypes/topic.publish"
    resource   = google_pubsub_topic.backup-topic.name
  }

  timeout     = 80
  entry_point = "helloPubSub"

  environment_variables = {
    PUB_SUB_NAME = google_pubsub_topic.backup-topic.name
    TG_BOT_TOKEN = var.tg_bot_token
    TG_USER      = var.tg_user
  }
}

variable "gcp_region" {
  type        = string
  description = "GCP region"
}
variable "gcp_zone" {
  type        = string
  description = "GCP zone"
}
variable "gcp_project" {
  type        = string
  description = "GCP project name"
}

variable "tg_bot_token" {
  type      = string
  sensitive = true
}

variable "tg_user" {
  type      = string
  sensitive = true
}

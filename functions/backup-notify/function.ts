import got from "got";

export const helloPubSub = async (event: any) => {
  const { TG_BOT_TOKEN, TG_USER } = process.env;

  const { data, attributes } = event;
  const decoded = JSON.parse(Buffer.from(data, 'base64').toString());

  const { objectGeneration, overwroteGeneration, eventTime, eventType } = attributes;
  const { id, timeCreated, updated, size, md5Hash } = decoded;

  const msg = JSON.stringify({
    id,
    eventType,
    eventTime,
    objectGeneration,
    overwroteGeneration,
    timeCreated,
    updated,
    size,
    md5Hash
  });

  const res = await got.post(`https://api.telegram.org/bot${TG_BOT_TOKEN}/sendMessage?chat_id=${TG_USER}&text=${encodeURI(msg)}`);
  // console.log(res);
};

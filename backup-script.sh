#!/bin/bash
set -x

/root/google-cloud-sdk/bin/gcloud auth activate-service-account --key-file aarnihalinen-e0b7bb157759.json

if /root/google-cloud-sdk/bin/gsutil -m rsync -e -d -x ".*\.git/.*|.*\.zsh_history$|.*/node_modules/.*|.*\.oh-my-zsh/.*" -r \
/home/arska gs://aarnihalinen-home-server-backup ; then
  # Fill in <token> & <chat-id> to get msg after backup is done
  curl -X POST https://api.telegram.org/<token>/sendMessage\?chat_id=<chat-id>\&text=Backuped
else
  # Fill in <token> & <chat-id> to get msg after backup is done
  curl -X POST https://api.telegram.org/<token>/sendMessage\?chat_id=<chat-id>\&text=Backup%20failed
fi

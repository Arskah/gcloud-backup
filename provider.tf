terraform {
  required_version = ">= 0.12"
  backend "gcs" {
    bucket = "tf-state-aarnihalinen"
    prefix = "prod"
  }
}

provider "google" {
  project = var.gcp_project
  region  = var.gcp_region
  zone    = var.gcp_zone
}

provider "archive" {}

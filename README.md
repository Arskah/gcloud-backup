# GCloud Backup

[Possible idea](https://cloud.google.com/architecture/dr-scenarios-for-data)

## Installation

- Terraform apply
- Download Service Account JSON => Server
- Install gsutil to server /root https://cloud.google.com/storage/docs/gsutil_install#linux
- Setup Cron to run backup-script.sh on server
